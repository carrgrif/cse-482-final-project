import glob
import json
import os

def main():

    files = glob.glob("/user/research/ptan/data/Twitter/*")
    print(len(files))

    stored = {}
    index = 0
    for temp in files:
        stored[index] = temp
        index += 1

    # Keywords that vary based on the company we search tweets for
    keywords = ['Disney', '$DIS', 'Bob Iger', 'Walt Disney Company']
    months = {'Jan' : '01', 'Feb' : '02', 'Mar' : '03', 'Apr' : '04', 'May' : '05', 'Jun'\
        :'06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov':'11','Dec'\
            : '12'}
    result = open("DISResults.json", "a")
   
    counter = 0
    # Go by increments of 200 files to process in case of errors
    for i in range(0, 200):
        with open(stored[counter]) as f:
            tweets = [json.loads(line) for line in f if line != "\n"]
            data = {}
            for twt in tweets:
                if 'text' not in twt:
                    continue
                else:
                    for word in keywords:
                        if word in twt['text']:
                            date = twt['created_at'].split()
                            new_date = date[5] + "-" + months[date[1]] + "-" +\
                                       date[2]
                            data['date'] = new_date
                            data['tweet'] = twt['text']
                            json.dump(data, result)
                            result.write("\n\n")
                            break

        counter += 1
        print(counter)
        if counter == 200:
            break
    result.close()
    return 0

if __name__ == "__main__":
    main()
